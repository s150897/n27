# Klausur / Test 2 am 14.1.2020 um 9:10 Uhr 

1. Fehler finden und auf Papier dokumentieren
2. alles,was in T1 K1 relevant war
3. Selbst etwas ausprogrammieren (GUI oder server.js)
4. SQL. Evtl. einen SQL Befehl anhand einer gegebenen Dokumentation selbst erstellen.
5. if und else (auch verschachteln)
Bitte auch die alten if-else-Sachen anschauen im Training
6. Symmetrische uns asymmetrische Verschlüsselungen erklären / gegeneinander abgrenzen. Den Sinn jeweils erklären. Die Implentation am Rechner kurz beschreiben.





## Beispiel zu if und else: 

```Javascript
//Wenn ein Schüler nicht volljährig ist, wird "Eintritt verweigert".

var darfHinein = "nein"
var alter = 18;

(alter >= 18){
    darfHinein = "ja"
}

Console.Log("Der Schüler / die Schülerin darf hinein: " + darfHinein )

```

```Javascript
//Wenn ein Schüler nicht volljährig ist, wird "Eintritt verweigert".

var darfHinein = ""
var alter = 18;

(alter >= 18){
    darfHinein = "ja"
}else{
    darfHinein = "nein"
}

Console.Log("Der Schüler / die Schülerin darf hinein: " + darfHinein )

```

```Javascript
// Wenn ein Schüler nicht volljährig ist, wird "Eintritt verweigert".
// Schülerinnen zahlen 3 Euro.
// Schüler zahlen 4 Euro.

var darfHinein = true
var istVolljaerig = true;
var geschlecht = "w"

if(istVolljaerig){
    darfHinein = true
        Console.Log("Der Schüler / die Schülerin darf hinein. ")
}else{
    darfHinein = false
        Console.Log("Der Schüler / die Schülerin darf nicht hinein. ")
}

```


