/**
 *
 *Hier können sie IF und Else trainieren. 
 * Am besten dazu den nodemon starten:
 * 
 *     .\node_modules\.bin\nodemon Training\if-else-training.js
 * 
 *  Dann die Ausgabe im Terminal beobachten.
 */

 // Deklaration und Intialisierung 
 // Lies: "Der Wert 0 wird zugewiesen an 
 //        eine Variable namens x"

 let x= 0
 let y= 1

 if("GW11A".endsWith("A")||("GW11a".endsWith("B"))){
     console.log("Die Prüfung ist WAHR. Es werden die Anweisungen im Rumpf von IF aus geführt.")
 }else{
    console.log("Die Prüfung ist FALSCH. Es werden die Anweisungen im Rumpf von ELSE aus geführt.")
 }

// Fälle: 
 
//22. if("GW11A".endsWith("A")||("GW11A".endsWith("B"))) 

//21. if("GW11A". endsWith("A")) Der Ausdruck ist Wahr. Die Funktion prüft, ob eine Zeichenkette mit einer anderen Zeichenkette endet.

//20. if("GW11A". includes("11")) Der Ausdruck ist Wahr. Die Funktion prüft, ob eine Zeichenkette eine andere Zeichenkette enthält.

//19. if("GW11A". startswith("G")) Der Ausdruck ist Wahr. Die Funktion prüft ob eine Zeichenkette mit einer anderen Zeichenkette beginnt.

//18. if("GW11A".lenght === 5) Der Ausdruck ist Falsch, die Eigenschaft lenght gibt die Anzahl der Zeichen einer Zeichenkette zurück.

//17. if(undefined) Der Ausdruck ist Falsch     Ein Objekt ist undefined, wenn sie nicht exestiert. 

//16. if(1 > 2 || 1 == 1) Der Ausdruck ist wahr, weil einer der beiden Prüfungen wahr ist.

//15. if(1 > 2 && 1== 1) Der Ausdruck ist Falsch, weil die erste Prüfung Falsch ist. Alle mit && verketteten Prüfungen müssen wahr sein, damit die Prüfung insgesamt Wahr wird.

//14. if(false) Der Ausdruck ist FALSCH.

//13. if(true) Ausdruck ist WAHR

//12. if(0) Die Zahl 0 ist FALSCH.

//11. if(5) Zahlen sind WAHR bis auf 0.

//10. if("")    leere Zeichenketten in javascript sind FALSCH.

// 9. if("a")   nicht leere Zeichenketten in javascript sind WAHR.

// 8. if(1 != 0) Vergleichsoperator "ungleich"  Der Ausdruck ist WAHR.

// 7. if(1 <= 1)  Vergleichsoperator "kleiner als"  Der Ausdruck ist WAHR.

// 6.  if(1 >= 1) Vergleichsoperator "größer gleich"   Der Ausdruck ist WAHR.

// 5. if(1 === "1") Vergleichsoperator "gleich"     Der Ausdruck ist FALSCH
//                  Das 3fache "===" prüft auf die Gleichheit des Wertes und des Typs. 
//                  "1" : string/ Zeichenkette
//                   1  : Zahl 

// 4. if(1 == "1") Verleichsoperator "gleich"      Der Ausdruck ist WAHR
//                 Das doppelte "==" prüft auf die Gleichheit des Wertes.
//                 aber nicht auf Gleichheit des Typs.

// 3. if(0 == 1) Vergleichsoperator: "gleich"     Der Ausdruck ist FALSCH

// 2. if(0 > 1) Vergleichsoperator: "größer als"  Der Ausdruck ist Falsch

// 1. if(0 < 1) Vergleichsoperator: "kleine als"  Der Ausdruck ist WAHR 